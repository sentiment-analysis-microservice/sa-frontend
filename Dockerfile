FROM node:18 AS builder

ENV BACKEND_URL=http://backend.rahul.webnex.us

WORKDIR /app

COPY . .
# install node modules and build assets
RUN npm install

RUN echo "REACT_APP_BACKEND=${BACKEND_URL}" > .env

RUN npm run build

FROM nginx:alpine
# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html
# Remove default nginx static assets
RUN rm -rf ./*
# Copy static assets from builder stage
COPY --from=builder /app/build .

ENTRYPOINT ["nginx", "-g", "daemon off;"]